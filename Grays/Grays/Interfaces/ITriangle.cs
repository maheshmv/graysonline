﻿namespace Grays.Interfaces
{
    public interface ITriangle
    {
        double IsValidTriangle(int sideA, int sideB, int sideC);
    }
}
