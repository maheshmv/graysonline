﻿namespace Grays.Interfaces
{
    public interface ICommonElements
    {
        int[] GetCommonElements(int[] array);
    }
}
