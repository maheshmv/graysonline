﻿using System;
using System.Collections.Generic;

namespace Grays.Interfaces
{
    public interface IDivisors
    {
        List<int> PositiveDivisors(int Num);
    }
}
