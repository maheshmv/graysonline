﻿using System;
using Grays.Interfaces;

namespace Grays
{
    public class Triangle : ITriangle
    {
        public double IsValidTriangle(int SideA, int SideB, int SideC)
        {
            if (SideA <= 0 || SideB <= 0 || SideC <= 0)
                throw new InvalidTriangleException("Negative side is not valid");

            else if (SideC >= SideA + SideB)
                throw new InvalidTriangleException("Invalid sides [Side A + Side B <= Side C]");
            else if (SideB >= SideA + SideC)
                throw new InvalidTriangleException("Invalid sides [Side A + Side C <= Side B]");
            else if (SideA >= SideB + SideC)
                throw new InvalidTriangleException("Invalid sides [Side C + Side B <= Side A]");

            int P = (SideA + SideB + SideC) / 2;
            double area = Math.Sqrt(P * (P - SideA) * (P - SideB) * (P - SideC));
            return area;
        }

    }
        public class InvalidTriangleException : Exception
        {
            public InvalidTriangleException(string msg) : base(msg)
            {

            }
        }
}
