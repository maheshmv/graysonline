﻿using System;
using System.Collections.Generic;
using Grays.Interfaces;
using System.Linq;

namespace Grays
{
    public class CommonElements : ICommonElements
    {
        private int[] arr;
        public int[] GetCommonElements(int[] array)
        {
            if (array is null)
                return new int[] { };
            arr = array;
            Dictionary<int, int> dict = new Dictionary<int, int>();
            InserttoDictionary(dict);
            int max = 1;
            List<int> retList = new List<int>();
            foreach (KeyValuePair<int, int> pair in dict)
            {
                if (pair.Value > max)
                {
                    // ignore previous list, create new list of common keys
                    retList = new List<int> { pair.Key };
                    max = pair.Value;
                }
                else if (pair.Value == max)
                {
                    retList.Add(pair.Key);
                }
            }
            return retList.ToArray();
        }

        private void InserttoDictionary(Dictionary<int, int> dict)
        {
            arr.ToList().ForEach(i => {
                if(dict.ContainsKey(i))
                {
                    dict[i]++;
                }
                else
                {
                    dict.Add(i, 1);
                }
            });

        }
    }
}
