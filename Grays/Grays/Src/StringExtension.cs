﻿using System;

namespace Grays
{
    public static class StringExtension
    {
        public static bool IsNullOrEmpty(this String s )
        {
            string str = s;
            bool ret = true;
            if (str?.Length > 0)
            {
                ret = false;
            }
            return ret;
        }
    }
}
