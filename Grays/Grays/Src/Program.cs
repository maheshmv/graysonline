﻿using Grays.Interfaces;
using System.Collections.Generic;
namespace Grays
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Console.WriteLine("Hello World!");

            string s = "null";
            Console.WriteLine(s.IsNullOrEmpty());
            s = null;
            Console.WriteLine(s.IsNullOrEmpty());
            s = "";
            Console.WriteLine(s.IsNullOrEmpty());

            IDivisors div = new Divisors();
            List<int> arr = div.PositiveDivisors(97);

            foreach(int i in arr)
            {
                Console.WriteLine(i);
            }

            ICommonElements com = new CommonElements();
            int [] arr=  com.GetCommonElements(new int[] { 1,2,3,4,5,6, 7, 8, 9, 10 });*/
            List<int> tables = new List<int> { 4,6,5,4,4,4};
            List<int> custGroups = new List<int>() {2,3,2,3,6,2,4,2,5,4};
            ReservationSystem.ReserveCustomers(tables, custGroups);
        }
    }
}
