﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Grays
{
    public static class ReservationSystem
    {

        /// <summary>
        /// Save the state to Partial filling of table.
        /// The best state will be saved and continued with.
        /// </summary>
        private class PartialFill
        {
            public int seated { get; set; }
            public List<int> Customers { get; set; }
            public List<int> Tables { get; set; }
        }

        /// <summary>
        /// Public function to reserve the Table for given list of Customers.
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="customers"></param>
        /// <returns></returns>
        public static int ReserveCustomers(List<int> tables, List<int> customers)
        {
            List<int> tableCap = new List<int>(tables);
            List<int> custdesc = customers.OrderByDescending(d=>d).ToList();
            int totalSeated = 0, seatedPartiallyOnTables = 0;
            foreach(int grp in custdesc.ToList())
            {
                if(tableCap.Contains(grp))
                {
                    tableCap.Remove(grp);
                    custdesc.Remove(grp);
                    totalSeated += grp;
                }
            }
            if (tableCap.Count>0 && custdesc.Count>0)
            {
                seatedPartiallyOnTables = BestSolution(tableCap, custdesc);
            }
            return totalSeated + seatedPartiallyOnTables;
        }

        /// <summary>
        /// This function is written with fill the table with customes when
        /// the group size of customers doesn't match exactly with the chairs available on table.
        /// We try to fill table partially but entire group will be seated together.
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="customers"></param>
        /// <returns></returns>
        private static int BestSolution(List<int> tables, List<int> customers)
        {
            int best = 0;
            List<int> lstTables = new List<int>(tables);
            List<int> lstCustomers = new List<int>(customers);
            int totalTables = lstTables.Count;
            int maxOneTable = 0, tIndex=0;  // max seating possible on one table
            List<int> bestCaseCustomersList = new List<int>();

            foreach(int tableSize in lstTables.ToList())
            //for (int tIndex = 0; tIndex < lstTables.Count; tIndex++)
            {
                //int tableSize=lstTables[tIndex];
                maxOneTable = 0;
                bestCaseCustomersList = null;
                if (lstCustomers.Count == 0)
                    return best;
                for (int i = 1; i <= tableSize; i++)
                {
                    PartialFill ret = _BestforPartialTableSize(lstTables, lstCustomers,i, tableSize);
                    
                    if (maxOneTable < ret.seated)
                    {
                        maxOneTable = ret.seated;
                        bestCaseCustomersList = ret.Customers;
                    }
                }
                best += maxOneTable;
                lstCustomers = bestCaseCustomersList;
                if (maxOneTable == tableSize)
                {
                    lstTables.Remove(tableSize);
                }
                else //
                {
                    lstTables[tIndex] -= maxOneTable;
                    tIndex++;
                }
            }
            return best;
        }

        /// <summary>
        /// Function to retun the seatings possible with a particular combination of chairs.
        /// Fill the seats = partSize and/or rest of the available seats.
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="customers"></param>
        /// <param name="partSize"></param>
        /// <param name="TotalSize"></param>
        /// <returns></returns>
        private static PartialFill _BestforPartialTableSize(List<int> tables, List<int> customers, int partSize, int TotalSize)

        {
            List<int> lstTables = new List<int>(tables);
            List<int> lstCustomers = new List<int>(customers);
            if (lstCustomers.Contains(partSize) && lstCustomers.Contains(TotalSize - partSize))
            {
                lstCustomers.Remove(partSize);
                lstCustomers.Remove(TotalSize - partSize);

                return new PartialFill  { seated= TotalSize, Customers = lstCustomers };
            }
            else if (lstCustomers.Contains(partSize))
            {
                lstCustomers.Remove(partSize);
                return new PartialFill { seated = partSize, Customers = lstCustomers };
            }
            else if (lstCustomers.Contains(TotalSize - partSize))
            {
                lstCustomers.Remove(TotalSize - partSize);
                int rem = (TotalSize - partSize);
                return new PartialFill { seated = rem, Customers = lstCustomers };
            }
            return new PartialFill { seated = 0, Customers = new List<int>() }; ;
        }
    }
}
