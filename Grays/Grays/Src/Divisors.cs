﻿using System;
using System.Collections.Generic;
using Grays.Interfaces;

namespace Grays
{
    public class Divisors : IDivisors
    {
        public List<int> PositiveDivisors(int Num)
        {
            if (Num < 1)
                return new List<int>();
            else if (Num == 1)
                return new List<int>() { 1 };
           
            if(IsPrime(Num))
            {
                return new List<int>() { 1, Num };
            }
            SortedSet<int> retList = new SortedSet<int>();
            int min = Num;
            int div = 0;
            for(int i =1;i<min;i++)
            {
                if(Num%i==0)
                {
                    div = Num / i;
                    retList.Add(i);
                    retList.Add(div);
                    min = div;
                }
            }
            
            return new List<int>(retList);
        }
        public static bool IsPrime(int num)
        {
            if (num <= 1) return false;
            if (num == 2) return true;
            if (num % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(num));

            for (int i = 3; i <= boundary; i += 2)
                if (num % i == 0)
                    return false;

            return true;
        }

    }
}
