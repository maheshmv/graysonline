using System;
using Xunit;
using Grays.Interfaces;

namespace Grays.Tests
{
    public class TriangleTests
    {
        struct TriangleSides
        {
            public int A { get; set; }
            public int B { get; set; }
            public int C { get; set; }
            public TriangleSides(int a, int b, int c)
            {
                A = a;
                B = b;
                C = c;
            }
        }

        ITriangle triangle = new Triangle();
        [Fact]
        void InvalidTwoSidesEqualThird()
        {
            TriangleSides side = new TriangleSides(2, 3, 5);// Invalid

            Assert.Throws<InvalidTriangleException>(() => triangle.IsValidTriangle(side.A, side.B, side.C));
        }
        [Fact]
        void NegativeSides()
        {
            TriangleSides side = new TriangleSides(-13, -15, -30);// Invalid

            Assert.Throws<InvalidTriangleException>(() => triangle.IsValidTriangle(side.A, side.B, side.C));
        }
        [Fact]
        void InvalidTwoSidesLessThanThird()
        {
            TriangleSides side = new TriangleSides(13, 15, 30);// Invalid

            Assert.Throws<InvalidTriangleException>(() => triangle.IsValidTriangle(side.A, side.B, side.C));
        }
        [Fact]
        void OneSideIsZero()
        {
            TriangleSides side = new TriangleSides(13, 0, 30);// Invalid

            Assert.Throws<InvalidTriangleException>(() => triangle.IsValidTriangle(side.A, side.B, side.C));
        }
        [Fact]
        void ValidTriangles()
        {
            TriangleSides side = new TriangleSides(30, 12, 30);// Valid

            //int p = (side.A + side.B + side.C) / 2;
            double _area = CalculateArea(side);// Math.Sqrt(p * (p - side.A) * (p - side.B) * (p - side.C));
            double area = triangle.IsValidTriangle(side.A, side.B, side.C);
            Assert.Equal<double>(_area, area);

            side.B = 50;
            side.C = 70;

            _area = CalculateArea(side);
            area = triangle.IsValidTriangle(side.A, side.B, side.C);
            Assert.Equal<double>(_area, area);
        }

        private double CalculateArea(TriangleSides t)
        {
            int p = (t.A + t.B + t.C) / 2;
            return Math.Sqrt(p * (p - t.A) * (p - t.B) * (p - t.C));
        }
    }
}
