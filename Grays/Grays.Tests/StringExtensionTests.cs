﻿using System;
using System.Collections.Generic;
using Xunit;
using Grays.Interfaces;

namespace Grays.Tests
{
    public class StringExtensionTests
    {
        [Fact]
        void NullCheck()
        {
            string str = null;
            Assert.True(StringExtension.IsNullOrEmpty(str));
        }
        [Fact]
        void ValidStringCheck()
        {
            string str = "Foo";
            Assert.False(StringExtension.IsNullOrEmpty(str));
        }
        [Fact]
        void EmptyStringCheck()
        {
            string str = "";
            Assert.True(StringExtension.IsNullOrEmpty(str));
        }
        [Fact]
        void NULLStringCheck()
        {
            string str = "null";
            Assert.False(StringExtension.IsNullOrEmpty(str));
        }
        [Fact]
        void UninitializedConcatenatedStringCheck()
        {
            string str = "" + "";
            String newStr = str;
            Assert.True(StringExtension.IsNullOrEmpty(str));
            Assert.True(StringExtension.IsNullOrEmpty(newStr));
        }
        [Fact]
        void IntToStringConvertCheck()
        {
            int str = 0;
            Nullable<int> MyInt = null;
            Assert.False(StringExtension.IsNullOrEmpty(Convert.ToString(str)));
            Assert.True(StringExtension.IsNullOrEmpty(MyInt.ToString()));
        }
    }
}
