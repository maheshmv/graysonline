﻿using System.Collections.Generic;
using Xunit;
using Grays.Interfaces;
using System.Linq;

namespace Grays.Tests
{
    public class DivisorTests
    {
        IDivisors divisors = new Divisors();
        List<int> list = new List<int>();
        [Fact]
        void ValidNumCheckAllDivides()
        {
            int num = 84;
            list = divisors.PositiveDivisors(num);
            Assert.All(list, (i) =>
            {
                int r = num % i;
                Assert.Equal(0, r);
            });
        }
        [Fact]
        void CheckZeroAndNegative()
        {
            int num = 0;
            list = divisors.PositiveDivisors(num);
            Assert.Empty(list);

            num = -180;
            list = divisors.PositiveDivisors(num);
            Assert.Empty(list);
        }
        [Fact]
        void CheckOne()
        {
            int num = 1;
            list = divisors.PositiveDivisors(num);
            Assert.Single(list);
            Assert.Equal(1, list.First());
        }
        [Fact]
        void CheckTwo()
        {
            int num = 2;
            list = divisors.PositiveDivisors(num);

            Assert.Equal(2, list.Count);
        }
        [Fact]
        void CheckSmallNum()
        {
            int num = 10;
            list = divisors.PositiveDivisors(num);
            Assert.Equal(4, list.Count); // 1,2,5,10
        }
        [Fact]
        void CheckPrimes()
        {
            int num = 7;
            list = divisors.PositiveDivisors(num);
            Assert.Equal(2, list.Count); // 1,7
            num = 4673;
            list = divisors.PositiveDivisors(num);
            Assert.Equal(2, list.Count); // 1,4673
        }
    }
}
